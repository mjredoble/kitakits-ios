//
//  ViewController.swift
//  KitaKits
//
//  Created by Michael Joseph Redoble on 09/07/2021.
//

import UIKit
import CoreLocation
import MapKit
import SwiftUI

class MainViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet private var mapView: MKMapView!
    @IBOutlet weak var menuView: UIView!
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.requestAlwaysAuthorization()
            locationManager?.requestWhenInUseAuthorization()
        }
        else {
            //TODO: Show message tracking location needs to allow
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways {
            
            self.currentLocation = locationManager?.location
            if let location = locationManager?.location {
                mapView.centerCoordinate = location.coordinate
                mapView.centerToLocation(location)
                
                let region = MKCoordinateRegion(
                      center: location.coordinate,
                      latitudinalMeters: 50000,
                      longitudinalMeters: 60000)
                mapView.setCameraBoundary(MKMapView.CameraBoundary(coordinateRegion: region), animated: true)
                
                let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 1000)
                mapView.setCameraZoomRange(zoomRange, animated: true)
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = location.coordinate
                annotation.title = "You"
                
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    @IBAction func createMeetup() {
        //Dismiss a SwiftUI View when called from a UIViewController
        
        let sessionView = SessionMenuView(parentView: self, dismissAction: { self.dismiss( animated: true, completion: nil) })
        let viewCtrl = UIHostingController(rootView: sessionView)
        self.present(viewCtrl, animated: true, completion: nil)
    }
    
    func startSession(meetup: Meetup) {
        //print(meetup.name ?? "")
        print("Lets call socketIO server here!")
    }
    
    //MARK: - MapView Delegates
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "MyMarker")
        annotationView.markerTintColor = UIColor(red: (69.0/255), green: (95.0/255), blue: (170.0/255), alpha: 1.0)
        return annotationView
    }
}

// MARK: - CoreLocation Delegate Methods
extension MainViewController: CLLocationManagerDelegate {
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        //
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            self.currentLocation = location
            print(location)
        }
    }
}
