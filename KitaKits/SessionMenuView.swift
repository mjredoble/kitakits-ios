//
//  SessionMenuView.swift
//  KitaKits
//
//  Created by Michael Joseph Redoble on 09/07/2021.
//

import SwiftUI

struct SessionMenuView: View {
    
    @State private var meetupName = ""
    @State private var password = ""
    @State private var setPassword = false
    @State private var selectedMeetupType = ""
    
    var parentView: MainViewController
    var dismissAction: (() -> Void)
    
    let meetupTypes = [MeetupType.Convoy, MeetupType.Chaperone] //Race, Meetup etc.
    
    var body: some View {
        Form {
            Section(header: Text("Create Meetup")) {
                HStack {
                    Text("Name:")
                    TextField("Enter name of meetup", text: $meetupName)
                }
                
                HStack {
                    Toggle("Set meetup password", isOn: $setPassword.animation())
                }
                
                if setPassword {
                    SecureField("Enter a password", text: $password)
                }
            }
            
            Section(header: Text("Meetup Type")) {
                //$ - signifies two-way binding in SwiftUI
                Picker("", selection: $selectedMeetupType) {
                    ForEach(meetupTypes, id: \.self) {
                        Text("\($0.rawValue)")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            Section {
                HStack(alignment: .center, spacing: 10, content: {
                    Spacer()
                    Button("Start Meetup") {
                        let newMeetup = Meetup(name: meetupName, type: MeetupType(rawValue: selectedMeetupType) ?? MeetupType.Chaperone, password: password)
                        newMeetup.name = meetupName
    
                        parentView.startSession(meetup: newMeetup)
                        
                        dismissAction()
                    }
                    .font(.headline)
                    
                    Spacer()
                })
            }
        }
    }
}

struct SessionMenuView_Previews: PreviewProvider {
    static var previews: some View {
        SessionMenuView(parentView: MainViewController(), dismissAction: {})
    }
}
