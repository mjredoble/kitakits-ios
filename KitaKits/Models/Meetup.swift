//
//  Meetup.swift
//  KitaKits
//
//  Created by Michael Joseph Redoble on 13/07/2021.
//

import Foundation

enum MeetupType: String {
    case Convoy = "Convoy"
    case Chaperone = "Chaperone"
}

class Meetup {
    var name: String
    var type: MeetupType
    var members: [String]
    
    var password: String?
    
    init(name: String, type: MeetupType, password: String?) {
        self.name = name
        self.type = type
        self.password = password
        
        self.members = [String]()
        self.members.append("You")
    }
}
